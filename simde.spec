%global commit_simde fefc7857ff3e785b988a61a8f5f3c5bd5eb24342
%global hedley_version 14
%global debug_package %{nil}

Name:          simde
Version:       0.8.2
Release:       1
Summary:       Implementations of SIMD instruction sets for systems which don't natively support them
License:       MIT and CC0-1.0
URL:           https://github.com/simd-everywhere/simde
Source0:       https://github.com/simd-everywhere/%{name}/archive/v%{version}.tar.gz
Source1:       ci.sh
BuildRequires: clang
BuildRequires: cmake
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: meson time

# simde/hedley.h
# https://github.com/nemequ/hedley
Provides: bundled(hedley) = %{hedley_version}

%description
%{summary}
The SIMDe header-only library provides fast, portable implementations of SIMD
intrinsics on hardware which doesn't natively support them, such as calling
SSE functions on ARM. There is no performance penalty if the hardware supports
the native implementation (e.g., SSE/AVX runs at full speed on x86,
NEON on ARM, etc.).

%package devel
Summary: Header files for SIMDe development
Provides: %{name}-static = %{version}-%{release}

%description devel
The simde-devel package contains the header files needed
to develop programs that use the SIMDe.

%prep
%autosetup -n %{name}-%{version} -p1

%build

%install
mkdir -p %{buildroot}%{_includedir}
cp -a simde %{buildroot}%{_includedir}

%check
# Check version.
version_major=$(grep '^#define SIMDE_VERSION_MAJOR ' simde/simde-common.h | cut -d ' ' -f 3)
version_minor=$(grep '^#define SIMDE_VERSION_MINOR ' simde/simde-common.h | cut -d ' ' -f 3)
version_micro=$(grep '^#define SIMDE_VERSION_MICRO ' simde/simde-common.h | cut -d ' ' -f 3)
test "%{version}" = "${version_major}.${version_minor}.${version_micro}"

for file in $(find simde/ -type f); do
  if ! [[ "${file}" =~ \.h$ ]]; then
    echo "${file} is not a header file."
    false
  elif [ -x "${file}" ]; then
    echo "${file} has executable bit."
    false
  fi
done

# Only test the GCC and Clang cases with RPM build flags.
# If you find test failures on the cases, reproduce it in the upstream CI.
# in the O2 or RPM build flags cases, and report it.
# See <https://github.com/simd-everywhere/simde/blob/master/.packit/>.
# Copy to use the modified script.
cp -p "%{SOURCE1}" ci.sh
sed -i -e '/^cat \/proc\/cpuinfo/ s/^/#/' ci.sh
sed -i -e '/^cat \/proc\/meminfo/ s/^/#/' ci.sh
sed -i -e '/^pip3 install meson/ s/^/#/' ci.sh
sed -i -e '/^IGNORE_EXIT_STATUS=/,/^SKIP_i686_GCC_DEFAULT=/ s/^/#/' ci.sh
diff -u "%{SOURCE1}" ci.sh || :

cp -p ci.sh ci_gcc.sh
IGNORE_EXIT_STATUS=
# Prepare the configuration.
cat > config.txt <<EOF
IGNORE_EXIT_STATUS=${IGNORE_EXIT_STATUS}
SKIP_ALL_GCC_DEFAULT=true
SKIP_ALL_GCC_O2=true
SKIP_ALL_GCC_RPM=
SKIP_ALL_CLANG_DEFAULT=true
SKIP_ALL_CLANG_O2=true
SKIP_ALL_CLANG_RPM=true
EOF
# Insert the configuration.
sed -i -e '/^#SKIP_i686_GCC_DEFAULT=/r config.txt' ci_gcc.sh
# Print the difference on the debugging purpose.
diff -u ci.sh ci_gcc.sh || :

cp -p ci.sh ci_clang.sh
IGNORE_EXIT_STATUS=true
# Prepare the configuration to be inserted.
cat > config.txt <<EOF
IGNORE_EXIT_STATUS=${IGNORE_EXIT_STATUS}
SKIP_ALL_GCC_DEFAULT=true
SKIP_ALL_GCC_O2=true
SKIP_ALL_GCC_RPM=true
SKIP_ALL_CLANG_DEFAULT=true
SKIP_ALL_CLANG_O2=true
SKIP_ALL_CLANG_RPM=
EOF
# Insert the configuration.
sed -i -e '/^#SKIP_i686_GCC_DEFAULT=/r config.txt' ci_clang.sh
# Print the difference on the debugging purpose.
diff -u ci.sh ci_clang.sh || :

%global toolchain clang
export CI_CLANG_RPM_CFLAGS="%{build_cflags} -fno-strict-aliasing"
export CI_CLANG_RPM_CXXFLAGS="%{build_cxxflags} -fno-strict-aliasing"
export CI_CLANG_RPM_LDFLAGS="%{build_ldflags}"
%global toolchain gcc
export CI_GCC_RPM_CFLAGS="%{build_cflags} -fno-strict-aliasing"
export CI_GCC_RPM_CXXFLAGS="%{build_cxxflags} -fno-strict-aliasing"
export CI_GCC_RPM_LDFLAGS="%{build_ldflags}"

# Run tests.
chmod u+x ./ci_gcc.sh ./ci_clang.sh
/bin/time -f '=> [%E]' ./ci_gcc.sh
/bin/time -f '=> [%E]' ./ci_clang.sh

%files devel
%license COPYING
%doc README.md
%{_includedir}/%{name}

%changelog
* Thu Dec 19 2024 xu_ping <707078654@qq.com> - 0.8.2-1
- Update to 0.8.2
  * Drop i386 (i686) support. 
  * update list of fully implemented instruction sets.
  * Add gcc arm 32bit armv8-a test in CI 
  * build for AMD Buildozer version 2
  * disable some FCVTZS/FCVTMS/FCVTPS/FCVTNS family intrinsics
  * Add neon2rvv support in vand series intrinsics
  * detect clang versions 18 & 19

* Mon Sep 30 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 0.8.0-1
- Update to 0.8.0

* Sat Jul 15 2023 yaoxin <yao_xin001@hoperun.com> - 0.7.6-1
- Update to 0.7.6 for fix build error caused by clang-15

* Tue Mar 8 2022 yaoxin <yaoxin30@huawei.com> - 0.7.3-1
- Upgrade simde to 0.7.3 to resolve compilation failures.

* Fri Jan 8 2021 chengzihan <chengzihan2@huawei.com> - 0.5.0-1
- Package init
